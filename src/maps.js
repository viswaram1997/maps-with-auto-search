import React from "react"
const { compose,withProps } = require("recompose");
const FaAnchor = require("react-icons/lib/fa/anchor");
const {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow,
} = require("react-google-maps");

const MapWithAMarker = compose(
  withProps({
    googleMapURL:'https://maps.googleapis.com/maps/api/js?key=AIzaSyCAqhpdJM_PDUI58_NuJT3FSu_JpL652LE&v=3.exp&libraries=geometry,drawing,places',
    loadingElement:<div style={{ height: `100%` }} />,
    containerElement:<div style={{ height: `400px` }} />,
    mapElement:<div style={{ height: `100%` }} />
  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <GoogleMap
    defaultZoom={16}
    defaultCenter={{lat:props.lat.lat,lng:props.lat.lng}}
  >
    <Marker
      position={{lat:props.lat.lat,lng:props.lat.lng }}
    >
     {props.isOpen && <InfoWindow onCloseClick={props.onToggleOpen}>
        <FaAnchor />
      </InfoWindow>}
    </Marker>
  </GoogleMap>
);

<MapWithAMarker
 
/>
export default MapWithAMarker;