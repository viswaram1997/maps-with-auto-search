import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import MapWithASearchBox from "./maps";
import Content  from './contenteditable'
import MapF from "./map search"
ReactDOM.render(<MapF />, document.getElementById('root'));
registerServiceWorker();
