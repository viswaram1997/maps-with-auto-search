import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import MapWithASearchBox from "./maps"
import Box from "./box"
class App extends Component {
  constructor(props){
    super(props)
    this.state={
      prop:{ lat: 13.0826802, lng: 80.27071840000008 }
    }
    this.getLocation=this.getLocation.bind(this)
  }  
  getLocation(data){
    var temp={
      lat:data[0].geometry.location.lat(),
      lng:data[0].geometry.location.lng()
    }
    console.log(data)
  setTimeout(() => {
    this.setState({
      prop:temp
    })
  }, 0);
  }
  render() {    
    return (
      <div className="App">
       <Box getLocation={this.getLocation}/>
       <MapWithASearchBox lat={this.state.prop} key={this.state.prop.lat}/>
      </div>
    );
  }
}

export default App;
